<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('User_table_model');

    }

    /*
     * Listing of student_registration
     */
    function index()
    {
        if ($this->session->userdata('log_in')) {
            return redirect('home_controller');
        }
        $this->load->view('login/login_form');

    }

    public function get_login()
    {


        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'User Name', 'required|trim');

        $this->form_validation->set_rules('user_password', 'Password', 'required');

//$this->form_validation->set_error_delimiters('<span class="error">', '</span>');

        if (!($this->form_validation->run())) {

            redirect('login_controller/index');

        } else {

            $user_name = $this->input->post('user_name');
            $password = $this->input->post('user_password');


            $userData = $this->User_table_model->login_check($user_name, $password);


//            $type = $this->User_model->login_type($emp_email_address, $emp_user_type);
            // $this->session->set_userdata('emp_email_address',$emp_email_address); // Session data

            // $sess_array = array();



            if ($userData > 0) {
                foreach ($userData as $row) {


                    $this->session->set_userdata('id', $row->user_id);
                    $this->session->set_userdata('user_name', $row->user_name);
                    $this->session->set_userdata('full_name', $row->full_name);
                    $this->session->set_userdata('mobile_no', $row->mobile_no);
                    $this->session->set_userdata('user_type', $row->user_type_id);

                    $this->session->set_userdata('log_in', true);
                }
                $this->session->set_flashdata('message', 'Welcome!');
                return redirect('home_controller/index');
            } else {
                $this->session->set_flashdata('error', 'Invalid Username/Password.');
                return redirect('login_controller/index');
            }
        }
    }

    public function logout()
    {
//        $employee_id = $this->session->userdata('employee_id');
//        $this->User_model->insert_logout($employee_id);
        $this->session->unset_userdata('log_in');
        $this->session->sess_destroy();
        return redirect('login_controller/index');
    }

    function change_password($user_name)
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('opassword', 'JOld Password', 'required|min_length[6]');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|min_length[6]');
        $this->form_validation->set_rules('confirmpassword', 'Confirm New Password', 'required|min_length[6]');
//        print_r($_POST);exit();
        if ($this->form_validation->run()) {
            $password = $this->input->post('opassword');

            $userServiceObj = new Member_service();
            $passcheck = $userServiceObj->checkpassword($password);
            if ($passcheck <> 0) {
                $update = $userServiceObj->updatepassword($user_name);
                $this->session->set_flashdata('message', 'Sucessfully Updated.');
                redirect('login_controller/change_password/' . $user_name);
            } else {
                $this->session->set_flashdata('error', 'Old Password Missmatch.');
                redirect('login_controller/change_password/' . $user_name);

            }

        } else {

            $data['content'] = 'login/change_password';
            $this->load->vars($data);
            $this->load->view('layout/main_layout');
        }
    }

}
