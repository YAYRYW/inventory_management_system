<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_controller extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('log_in')) {
            return redirect('login_controller');
        }
        $this->load->model('Invoice_model');
        $this->load->model('Order_table_model');
        $this->load->model('Payment_table_model');
        $this->load->model('User_table_model');
        $this->load->model('Customer_table_model');
        $this->load->model('Product_table_model');
        $this->load->model('Inventory_table_model');
        $this->load->model('service/Product_service');
        $this->load->model('Daily_expense_table_model');


    }


    public function index()
    {
        $data['invoice'] = $this->Invoice_model->get_all_invoice_data();
        $data['inventory_table'] = $this->Inventory_table_model->get_five_inventory_table_data();
        $data['title'] = "Home";
        $this->load->vars($data);
        $this->load->view('layout/home_layout');
    }


}
