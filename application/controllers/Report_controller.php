<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_controller extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
//        $this->load->model('Daily_sales_table_model');
//        $this->load->model('Daily_sales_details_table_model');
//        $this->load->model('Delivery_man_table_model');
//        $this->load->model('Shop_due_details_id_model');
//        $this->load->model('Shop_table_model');
        $this->load->model('Product_table_model');
        $this->load->model('service/Daily_sales_service');
    }

    /*
     * Listing of daily_sales_table
     */
    function index()
    {

    }

    function viewReport()
    {
//        print_r($_POST);die();
        $today = date('Y-m-d');
        $fromDate = $this->input->post('from_date');
        $toDate = $this->input->post('to_date');

        if ($fromDate == null && $toDate == null) {
            $fromDate = $today;
            $toDate = $today;

            $reportObj = New Daily_sales_service();
            $data['report_data'] = $reportObj->GetReportData($fromDate, $toDate);
            $data['exp_data'] = $reportObj->GetExpenseData($fromDate, $toDate);
            $data['purchase_data'] = $reportObj->GetPurchasePrice($fromDate, $toDate);
            $data['title'] = "Report";
            $data['content'] = 'report/report_view';
            $this->load->vars($data);
            $this->load->view('layout/main_layout');
        } else {
            $fromDate = $this->input->post('from_date');
            $toDate = $this->input->post('to_date');
            $reportObj = New Daily_sales_service();
            $data['report_data'] = $reportObj->GetReportData($fromDate, $toDate);
            $data['exp_data'] = $reportObj->GetExpenseData($fromDate, $toDate);
            $data['purchase_data'] = $reportObj->GetPurchasePrice($fromDate, $toDate);
            $data['title'] = "Report";
            $data['content'] = 'report/report_view';
            $this->load->vars($data);
            $this->load->view('layout/main_layout');
        }
    }


}
