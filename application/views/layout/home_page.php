<style>


    article .progress-group:nth-child(1) .progress .progress-bar {
        background: #dd526f;
    }

    article .progress-group:nth-child(2) .progress .progress-bar {
        background: #8954c7;
    }

    article .progress-group:nth-child(3) .progress .progress-bar {
        background: green;
    }

    article .progress-group:nth-child(4) .progress .progress-bar {
        background: orange;
    }

    article .progress-group:nth-child(5) .progress .progress-bar {
        background: #00acd6;
    }


</style>

<!--<script src="--><?php //echo base_url() ?><!--plugins/morris/morris.min.js"></script>-->
<!--<link rel="stylesheet" href="--><?php //echo base_url() ?><!--plugins/morris/morris.css">-->
<script src="<?php echo base_url() ?>js/demo.js"></script>

<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php $this->load->view('/flashMessage'); ?>
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">


        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-user-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Customer</span>
                        <span class="info-box-number">
                            <?php
                            $customerobj = new Customer_table_model();
                            $num = $customerobj->get_customer_number();
                            echo $num;
                            ?>

                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Invest</span>
                        <span class="info-box-number">
                            <?php
                            $investobj = new Inventory_table_model();
                            $invest = $investobj->purchase_amount();
                            echo $invest['total'];

                            ?>

                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-plus-square"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Gross Income</span>
                        <span class="info-box-number">
                            <?php
                            $gross = new Invoice_model();
                            $income = $gross->GetIncome();
                            echo $income['grossAmount']
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-cube"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Expense</span>
                        <span class="info-box-number">

                            <?php
                            $expObj = new Daily_expense_table_model();
                            $expense = $expObj->getExpense();
                            echo $expense['expense'];
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


        <div class="row">
            <div class="col-md-12">
                <div class="box">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                <!-- TABLE: LATEST ORDERS -->
                                <div class="box box-info">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Latest Orders</h3>


                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="table-responsive">
                                            <table class="table no-margin">
                                                <thead>
                                                <tr>
                                                    <th>Invoice No</th>
                                                    <th>Customer</th>
                                                    <th>Status</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($invoice as $i) { ?>
                                                    <tr>
                                                        <td>
                                                            <a href="<?php echo base_url('invoice_controller/viewInvoiceById/' . $i['invoice_no']) ?>"><?php echo $i['invoice_no']; ?></a>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $cusid = $i['customer_id'];
                                                            $cusObj = new Customer_table_model();
                                                            $cusInfo = $cusObj->get_customer_info($cusid);
                                                            echo $cusInfo['full_name'];


                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $status = $i['status'];

                                                            if ($status == 1) {


                                                                ?>


                                                                <span class="label label-success">Paid</span>
                                                                <?php

                                                            } else {
                                                                ?>
                                                                <span class="label label-danger">Due</span>
                                                            <?php }

                                                            ?>


                                                        </td>

                                                    </tr>
                                                <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer clearfix">
                                        <a href="<?php echo site_url("invoice_controller/add"); ?>"
                                           class="btn btn-sm btn-info btn-flat pull-left">Place
                                            New Order</a>
                                        <a href="<?php echo site_url("invoice_controller/index"); ?>"
                                           class="btn btn-sm btn-default btn-flat pull-right">View
                                            All Orders</a>
                                    </div>
                                    <!-- /.box-footer -->
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->

                            <!-- /.col -->
                            <div class="col-md-4 ">
                                <div class="box box-info">
                                    <div class="box-header with-border">
                                        <p class="text-center">
                                            <strong>Inventory</strong>
                                        </p>

                                        <article>
                                            <?php foreach ($inventory_table as $i) { ?>

                                                <div class="progress-group">
                                <span class="progress-text">
                                <?php $productId = $i['product_id'];
                                $proObj = new Product_table_model();
                                $name = $proObj->get_product_table($productId);
                                echo $name['product_name'];
                                ?>
                                </span>
                                                    <span class="progress-number">

                                    <?php
                                    $productId = $i['product_id'];
                                    $invObj = new Product_service();
                                    $soldProqty = $invObj->getSoldReturnedDamageProductAmount($productId);
                                    $proQty = $invObj->getTotalProductQuantityByProductId($productId);

                                    $sold = $soldProqty['TotalSold'];
                                    $Qty = $proQty['totalProduct'];


                                    if ($sold == null) {
                                        $sold = "00";
                                    }

                                    echo "Sold: " . $sold . "<sub>kg</sub> &nbsp;/  Total: " . $Qty . "<sub>kg</sub>";
                                    $percentageValue = $invObj->Getpercentage($sold, $Qty);

                                    ?>

                                </span>

                                                    <div class="progress sm">
                                                        <div class="progress-bar "
                                                             style="width: <?php echo $percentageValue; ?>"></div>
                                                    </div>
                                                </div>

                                            <?php } ?>
                                            <div class="box-footer clearfix">
                                                <a href="<?php echo site_url("Inventory_controller/add"); ?>"
                                                   class="btn btn-sm btn-warning btn-flat pull-left"> Adjust
                                                    Inventory </a>
                                                <a href="<?php echo site_url("Inventory_controller/store"); ?>"
                                                   class="btn btn-sm btn-default btn-flat pull-right"> View Store</a>
                                            </div>
                                        </article>
                                        <!-- /.progress-group -->
                                    </div>
                                </div>
                            </div>

                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- ./box-body -->

                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


</div>


</section>


</div>
<!-- /.content-wrapper -->

<script>
    $(function () {
        var front = $('.Front'),
            others = ['Left2', 'Left', 'Right', 'Right2'];

        $('.Carousel').on('click', '.Items', function () {
            var $this = $(this);

            $.each(others, function (i, cl) {
                if ($this.hasClass(cl)) {
                    front.removeClass('Front').addClass(cl);
                    front = $this;
                    front.addClass('Front').removeClass(cl);
                }
            });
        });
    });

</script>

<script src="<?php echo base_url() ?>js/pages/dashboard.js"></script>

