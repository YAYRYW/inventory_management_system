<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">


        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
                <a href="<?php echo base_url() ?>home_controller/index">
                    <i class="fa fa-home"></i> <span>Home</span>
                    <span class="pull-right-container">

            </span>
                </a>

            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa  fa-user-plus "></i> <span>Customer</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url("Customer_table_controller/add"); ?>"><i
                                    class="fa  fa-plus-square"></i> Add </a></li>
                    <li><a href="<?php echo site_url("Customer_table_controller/index"); ?>"><i
                                    class="fa fa-file-text"></i> View </a></li>

                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa  fa-cart-plus "></i> <span>Order</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url("invoice_controller/add"); ?>"><i class="fa  fa-plus-square"></i>
                            New Order</a></li>
                    <li><a href="<?php echo site_url("invoice_controller/index"); ?>"><i class="fa fa-file-text"></i>
                            View Order</a></li>
                    <li><a href="<?php echo site_url("payment_table_controller/index"); ?>"><i
                                    class="fa fa-file-text"></i> View Payments</a></li>

                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money"></i> <span>Daily Expense</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url("daily_expense_controller/add"); ?>"><i
                                    class="fa  fa-plus-square"></i> Add </a></li>
                    <li><a href="<?php echo site_url("daily_expense_controller/index"); ?>"><i
                                    class="fa  fa-bullseye"></i> View </a></li>
                </ul>
            </li>

            <?php if ($this->session->userdata('user_type') == 1) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa  fa fa-university"></i> <span>Inventory</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url("inventory_controller/add"); ?>"><i
                                        class="fa fa-plus-circle"></i> Adjust Inventory </a></li>
                        <li><a href="<?php echo site_url("inventory_controller/index"); ?>"><i class=" fa fa-list"></i>
                                Product Purchase History </a></li>
                        <li><a href="<?php echo site_url("inventory_controller/store"); ?>"><i
                                        class="fa fa fa-bullseye"></i> View store </a></li>


                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cubes"></i> <span>Product</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url("Product_controller/add"); ?>"><i
                                        class="fa fa-plus-circle"></i> New Product </a></li>
                        <li><a href="<?php echo site_url("Product_controller/index"); ?>"><i class=" fa fa-list"></i>
                                Product List </a></li>


                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i> <span>Report</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url("report_controller/viewReport"); ?>"><i
                                        class="fa  fa-bullseye"></i>View Report </a></li>

                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa  fa-users"></i>
                        <span>User</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url("user_controller/add"); ?>"><i class="fa  fa-user-plus"></i>
                                Add User</a></li>
                        <li><a href="<?php echo site_url("user_controller/index"); ?>"><i class="fa fa-group"></i> All
                                User</a></li>
                    </ul>
                </li>

            <?php } ?>

        </ul>

        <!-- /.sidebar -->
</aside>