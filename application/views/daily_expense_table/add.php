<link rel="stylesheet" type="text/css"
      href="<?php echo base_url() ?>plugins/datetimepicker/jquery.datetimepicker.min.css"/>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/select2/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Register
            <small>Register a new user</small>
        </h1>
        <?php $this->load->view('/flashMessage'); ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="register-box">
            <div class="register-box-body">
                <?php echo form_open('daily_expense_controller/add', ['id' => 'add', 'class' => 'form-horizontal']); ?>

                <div>Expense Details : <input type="text" class="form-control" name="expense_details"
                                              value="<?php echo $this->input->post('expense_details'); ?>" required/>
                </div>
                <div>Expense Amount : <input type="text" class="form-control" name="expense_amount"
                                             value="<?php echo $this->input->post('expense_amount'); ?>" required/>
                </div>
                <div>Expense Date : <input type="text" class="form-control" id="expense_date" name="expense_date"
                                           value="<?php echo $this->input->post('expense_date'); ?>" required/></div>
                <div>Expenses By : <select name="expenses_by" class="form-control" required>
                        <option value="">select user</option>
                        <?php
                        foreach ($all_user_table as $user_table) {
                            $selected = ($user_table['user_id'] == $this->input->post('expenses_by')) ? ' selected="selected"' : "";

                            echo '<option value="' . $user_table['user_id'] . '" ' . $selected . '>' . $user_table['full_name'] . '</option>';
                        }
                        ?>
                    </select></div>
                <br>

                <div class="row">
                    <div class="col-xs-8">
                        <span class="error"><?php echo validation_errors(); ?></span>
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                    </div>
                    <?php echo form_close(); ?>

                </div>
            </div>
            <!-- /.form-box -->
        </div>

    </section>
    <!-- /.content -->
</div>

<script>
    $('#expense_date').datetimepicker({
        format: 'Y-m-d',
        step: 30
    });

</script>

<script>
    $("#add").validate({});
</script>