!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
            <small>All Users</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>
                        <table id="userTable" class="table table-bordered table-striped">
                            <tr>
                                <th>Expense Id</th>
                                <th>Expense Details</th>
                                <th>Expense Amount</th>
                                <th>Expense Date</th>
                                <th>Expenses By</th>
                                <th>Logged In User Id</th>
                                <th>Actions</th>
                            </tr>
                            <?php foreach ($daily_expense_table as $d) { ?>
                                <tr>
                                    <td><?php echo $d['expense_id']; ?></td>
                                    <td><?php echo $d['expense_details']; ?></td>
                                    <td><?php echo $d['expense_amount']; ?></td>
                                    <td><?php echo $d['expense_date']; ?></td>
                                    <td><?php
                                        $Uid = $d['expenses_by'];
                                        $usrObj = new User_table_model();
                                        $info = $usrObj->get_user_table($Uid);
                                        echo $info['full_name'];
                                        ?></td>
                                    <td><?php
                                        $UsId = $d['logged_in_user_id'];
                                        $usInfo = $usrObj->get_user_table($UsId);
                                        echo $usInfo['full_name'];

                                        ?></td>
                                    <td>
                                        <a href="<?php echo site_url('daily_expense_controller/edit/' . $d['expense_id']); ?>">Edit</a>
                                        |
                                        <a href="<?php echo site_url('daily_expense_controller/remove/' . $d['expense_id']); ?>">Delete</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->