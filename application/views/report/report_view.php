<link rel="stylesheet" type="text/css"
      href="<?php echo base_url() ?>plugins/datetimepicker/jquery.datetimepicker.min.css"/>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/select2/select2.min.css">
!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Report
            <small>Daily Monthly Yearly Report</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>




                        <?php //print_r($exp_data); ?>
                        <section class="content"
                                 style="padding-top: 0px !important; padding-bottom: 0px !important; min-height: 10px !important;">

                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                <tr>

                                    <th>From</th>
                                    <th>To</th>
                                    <th></th>


                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <?php echo form_open('report_controller/viewReport'); ?>
                                    <td>
                                        <input type="text" value="<?php echo $this->input->post('from_date'); ?>"
                                               class="form-control" id="from" name="from_date"/>


                                    </td>
                                    <td>
                                        <input type="text" class="form-control"
                                               value="<?php echo $this->input->post('to_date'); ?>" id="to"
                                               name="to_date">

                                    </td>
                                    <td>
                                        <input type="submit" class="form-control btn btn-primary" value="Go">

                                    </td>
                                    <?php form_close(); ?>


                                </tr>
                                </tbody>
                            </table>


                            <table class="table table-bordered" style="margin-bottom: 0px !important;">
                                <tbody>
                                <tr>
                                    <td style="padding: 0px !important;">
                                        <label>Report</label>
                                    </td>
                                    <td style="padding: 0px !important;">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="box">
                                <div class="box-body">
                                    <table class="table table- table-" id="billtable">
                                        <thead>
                                        <tr>

                                            <th>Income</th>
                                            <th>Amount</th>

                                            <th>Expence</th>
                                            <th> Amount</th>

                                        </tr>
                                        </thead>

                                        <tbody>

                                        <tr>

                                            <td>Gross Sell</td>
                                            <td>
                                                <?php $saleAmount = $report_data['grossAmount'];
                                                echo $saleAmount; ?>

                                            </td>

                                            <td>Product Purchase</td>
                                            <td><?php $Amount = $purchase_data['purchaseAmount'];
                                                echo $Amount; ?></td>

                                        </tr>
                                        <tr>

                                            <td>Due</td>
                                            <td>
                                                <?php $dueAmount = $report_data['DueAmount'];
                                                echo $dueAmount; ?>
                                            </td>

                                            <td>Daily Expence</td>
                                            <td> <?php $dailyExp = $exp_data['expense_amount'];
                                                echo $dailyExp; ?></td>

                                        </tr>
                                        <tr>

                                            <th>Net Income</th>
                                            <th>
                                                <?php $netsaleAmount = $report_data['saleAmount'];
                                                echo $netsaleAmount; ?>
                                            </th>

                                            <th>Total Expence</th>
                                            <th><?php $totalliability = ($Amount + $dailyExp);
                                                echo $totalliability; ?> </th>

                                        </tr>


                                        </tbody>


                                    </table>


                                </div>
                                <!-- /.box-body -->
                            </div>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    $('#from').datetimepicker({
        format: 'Y-m-d',
        step: 30
    });
    $('#to').datetimepicker({
        format: 'Y-m-d',
        step: 30
    });

</script>