<link rel="stylesheet" type="text/css"
      href="<?php echo base_url() ?>plugins/datetimepicker/jquery.datetimepicker.min.css"/>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/select2/select2.min.css">
<script src="<?php echo base_url() ?>js\tinymce\js\tinymce\tinymce.min.js"></script>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ORDER
            <small>New order</small>
        </h1>
        <?php $this->load->view('/flashMessage'); ?>
    </section>
    <style>
        .float-left {
            float: left !important;
        }

        .float-right {
            float: right !important;
        }

        .margin-bottom {
            margin-bottom: 1em;
        }

        .margin-top {
            margin-top: 1em;
        }

        .margin-left {
            margin-left: 1em;
        }

        /* Form validation */
        #response {
            margin: 0 1em 1em 1em;
        }

        .paddingBottom {
            padding-bottom: 100px;
        }

        pre {
            width: 275px;
            hight: 100px;
            overflow: hidden;
            line-height: 2px;
        }


    </style>


    <main class="content">
        <?php echo form_open('invoice_controller/add', ['id' => 'invoice', 'class' => '']); ?>
        <div class="content_inner">

            <!--        <h1>Create invoice</h1>-->
            <!--        <hr>-->

            <div class="row">
                <div class="col-xs-6">
                    <h1>

                    </h1>
                </div>
                <div class="col-xs-6 text-right">
                    <h1>INVOICE</h1>

                </div>

            </div>

            <div class="row">
                <div class="col-xs-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a class="btn btn-xs btn-info pull-right" data-toggle="modal" data-target="#myModal"><i
                                        class="fa fa-plus"></i></a>
                            <h5>Customer: </h5>
                            <select name="customer_id" id="customer_id" class="form-control" required>
                                <option value="">select customer</option>
                                <?php
                                foreach ($all_customer_table as $customer_table) {
                                    $selected = ($customer_table['id'] == $this->input->post('customer_id')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $customer_table['id'] . '" ' . $selected . '>' . $customer_table['full_name'] . " " . "(" . $customer_table['company_name'] . ")" . '</option>';
                                }
                                ?>
                            </select>

                        </div>

                        <script type="text/javascript">
                            $("#customer_id").change(function () {
                                var selected = $("#customer_id").val();
                                dat = 'customer_id=' + selected;
                                var url = '<?php echo site_url("Customer_table_controller/get_customer_data_by_id");?>';
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: dat,
                                    success: function (data) {
                                        console.log(data);


                                        if (data == 'fail') {
                                            alert("there is no data");
                                        }
                                        else {


                                            var json = $.parseJSON(data);

                                            $("#customerName").text(json.full_name);
                                            $("#company").text(json.company_name);
                                            $("#address").text(json.address);
                                            $("#contact").text(json.contact_number);


                                        }
                                    }
                                });
                            });

                        </script>

                        <div class="panel-body form-group form-group-sm">
                            <div class="row">


                                <div class="col-md-12">

                                    <pre>
                                        <p id="customerName"></p>
                                        <b><p id="company"></p></b>
                                        <p id="address"></p>
                                        <p id="contact"></p>
                                    </pre>


                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-6 text-right">
                    <?php
                    $invObj = new Invoice_model();
                    $tableId = $invObj->lastInsertId();
                    $id = $tableId['id'];
                    $id++;
                    $today = date('ymd');

                    ?>

                    <div class="input-group col-xs-7 float-right margin-bottom">
                        <span class="input-group-addon">#</span>
                        <input type="text" id="invoice_id" name="invoice_no" class="form-control"
                               placeholder="Invoice Number" value="<?php echo $today . $id; ?>"
                               aria-describedby="sizing-addon1" required>
                    </div>
                    <div class="input-group col-xs-7 float-right margin-bottom">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" id="invoiceDate" name="invoice_date" class="form-control"
                               placeholder="Invoice Date"
                               aria-describedby="sizing-addon1" required>
                    </div>


                </div>
            </div>


            <table class="table table-bordered" id="invoice_table">
                <thead>
                <tr>
                    <th><a href="#" class="btn btn-success btn-xs add-row"><span class="glyphicon glyphicon-plus"
                                                                                 aria-hidden="true"></span></a></th>
                    <th width="250">
                        <h4> Product</h4>
                    </th>
                    <th>
                        <h4>Qty &nbsp;<sub>kg</sub></h4>
                    </th>
                    <th>
                        <h4>Price&nbsp;<sub>per kg</sub></h4>
                    </th>
                    <th width="100">
                        <h4>Discount</h4>
                    </th>
                    <th width="159">
                        <h4> Total</h4>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <a href="#" class="btn btn-danger btn-xs delete-row float-left"><span
                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>


                    </td>
                    <td>
                        <div class=" form-group form-group">

                            <div class=" form-group ">


                                <!--                                <input type="text" name="product[]" value="-->
                                <?php //echo $this->input->post('product'); ?><!--"-->
                                <!--                                       class="form-control" id="product"/>-->

                                <select name="product_id[]" class="form-control">
                                    <option value="">select product</option>
                                    <?php
                                    foreach ($all_product_table as $product_table) {
                                        $selected = ($product_table['product_id'] == $this->input->post('product_id')) ? ' selected="selected"' : "";

                                        echo '<option value="' . $product_table['product_id'] . '" ' . $selected . '>' . $product_table['product_name'] . '</option>';
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>
                    </td>
                    <td class="text-right">
                        <div class=" form-group form-group">
                            <input type="text" class="form-control calculate" name="invoice_product_qty[]" value="1"
                                   required>
                        </div>
                    </td>
                    <td class="text-right">
                        <div class="input-group input-group">
                            <span class="input-group-addon">৳</span>
                            <input type="text" class="form-control calculate" name="invoice_product_price[]"
                                   aria-describedby="sizing-addon1" placeholder="0.00" required>
                        </div>
                    </td>
                    <td class="text-right">
                        <div class=" form-group form-group">
                            <input type="text" class="form-control calculate" name="invoice_product_discount[]"
                                   value="0.00">
                        </div>
                    </td>
                    <td class="text-right">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">৳</span>
                            <input type="text" class="form-control calculate-sub" name="invoice_product_sub[]"
                                   id="invoice_product_sub" value="0.00" aria-describedby="sizing-addon1" readonly>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <div id="invoice_totals" class="row text-right">

                <div class="col-xs-4 col-xs-offset-8">
                    <table class="table table-bordered">
                        <tr>
                            <th>Sub Total:</th>
                            <th>

                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon">৳</span>
                                    <input type="text" id="subTotal" class="form-control" name="subTotal" value="0.00"
                                           readonly/>
                                </div>

                            </th>
                        </tr>
                        <tr>
                            <th>Discount:</th>
                            <th>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon">৳</span>
                                    <input type="text" id="discount" class="form-control" name="discount" value="0.00"
                                           readonly/>
                                </div>

                            </th>
                        </tr>
                        <tr>
                            <th>Paid:</th>
                            <th>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon">৳</span>
                                    <input type="text" id="paidAmount" class="form-control" name="paidAmount"
                                           placeholder="0.00" required/>
                                </div>


                            </th>
                        </tr>
                        <tr>
                            <th>Due:</th>
                            <th>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon">৳</span>
                                    <input type="text" id="due" class="form-control" name="due" value="0.00" readonly/>
                                </div>

                            </th>
                        </tr>
                    </table>

                </div>
            </div>

            <div class="">
                Remarks: <textarea class="form-control" name="remarks"></textarea>
            </div>
            <div class=" col-xs-offset-11 paddingBottom margin-top ">
                <?php echo validation_errors(); ?>
                <input type="submit" class="btn btn-success" value="Submit">
            </div>
        </div>
        <?php echo form_close(); ?>

    </main>
</div>


<?php echo form_open('customer_table_controller/add', ['id' => 'add', 'class' => 'form-horizontal']); ?>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Customer</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    <label for="company_name" class="col-md-4 control-label">Company Name</label>
                    <div class="col-md-8">
                        <input type="text" name="company_name" value="<?php echo $this->input->post('company_name'); ?>"
                               class="form-control" id="company_name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="full_name" class="col-md-4 control-label">Customer Name</label>
                    <div class="col-md-8">
                        <input type="text" name="full_name" value="<?php echo $this->input->post('full_name'); ?>"
                               class="form-control" id="full_name" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-md-4 control-label">Address</label>
                    <div class="col-md-8">
                        <input type="text" name="address" value="<?php echo $this->input->post('address'); ?>"
                               class="form-control" id="address" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="contact_number" class="col-md-4 control-label">Contact Number</label>
                    <div class="col-md-8">
                        <input type="text" name="contact_number"
                               value="<?php echo $this->input->post('contact_number'); ?>" class="form-control"
                               id="contact_number" required/>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>

<script>

    $(document).ready(function () {





        // remove product row
        $('#invoice_table').on('click', ".delete-row", function (e) {
            e.preventDefault();
            $(this).closest('tr').remove();
            calculateTotal();
        });

        // add new product row on invoice
        var cloned = $('#invoice_table tr:last').clone();
        $(".add-row").click(function (e) {
            e.preventDefault();
            cloned.clone().appendTo('#invoice_table');
        });

        calculateTotal();

        $('#invoice_table, #invoice_totals').on('change keyup paste', '.calculate', function () {
            updateTotals(this);
            calculateTotal();
        });

        function updateTotals(elem) {

            var tr = $(elem).closest('tr'),
                quantity = $('[name="invoice_product_qty[]"]', tr).val(),
                price = $('[name="invoice_product_price[]"]', tr).val(),
                isPercent = $('[name="invoice_product_discount[]"]', tr).val().indexOf('%') > -1,
                percent = $.trim($('[name="invoice_product_discount[]"]', tr).val().replace('%', '')),
                subtotal = parseInt(quantity) * parseFloat(price);

            if (percent && $.isNumeric(percent) && percent !== 0) {
                if (isPercent) {
                    subtotal = subtotal - ((parseFloat(percent) / 100) * subtotal);
                } else {
                    subtotal = subtotal - parseFloat(percent);
                }
            } else {
                $('[name="invoice_product_discount[]"]', tr).val('');
            }

            $('.calculate-sub', tr).val(subtotal.toFixed(2));
        }

        function calculateTotal() {

            var grandTotal = 0,
                disc = 0;

            $('#invoice_table tbody tr').each(function () {
                var c_sbt = $('.calculate-sub', this).val(),
                    quantity = $('[name="invoice_product_qty[]"]', this).val(),
                    price = $('[name="invoice_product_price[]"]', this).val() || 0,
                    subtotal = parseInt(quantity) * parseFloat(price);

                grandTotal += parseFloat(c_sbt);
                disc += subtotal - parseFloat(c_sbt);
            });

            // VAT, DISCOUNT, SHIPPING, TOTAL, SUBTOTAL:
            var subT = parseFloat(grandTotal),
                vat = parseInt($('.invoice-vat').attr('data-vat-rate')),
                c_ship = parseInt($('.calculate.shipping').val()) || 0,
                withShip = parseInt(subT + c_ship);


            $('#subTotal').val(subT.toFixed(2));
            $('#discount').val(disc.toFixed(2));


        }

    });

    $("#paidAmount").keyup(function () {


        var paidAmount = parseInt($('#paidAmount').val());
        var subTotal = parseInt($('#subTotal').val());

        var due = (subTotal - paidAmount);

        $("#due").val(due);


    });


    $('#invoiceDate').datetimepicker({
        format: 'Y-m-d',

    });

    $("#invoice").validate({});


</script>


</div>



