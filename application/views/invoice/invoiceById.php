<style>
    .marginTop {
        margin-top: 1em;

    }

    pre {
        width: 250px;
        hight: 120px;
        overflow: hidden;
    }
</style>
<link href="<?php echo base_url() ?><css/money_receipt.css" rel="stylesheet">


<div class="content-wrapper">

    <div id="print2">


        <style type="text/css" media="print">
            @page {
                size: auto;   /* auto is the initial value */
                margin: 0 0 0 0;  /* this affects the margin in the printer settings */

            }

            #printButton {
                display: none;
            }

            #header {
                display: none;
            }

            .printclass {
                height: 20mm;
            }

            pre {
                width: 250px;
                hight: 120px;
                overflow: hidden;
            }


        </style>

        <div class="printclass"></div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable"
               bgcolor="#e1e1e1">
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <td>
                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable"
                           bgcolor="#ffffff" style="border-radius: 10px 10px 0 0;">
                        <tr class="hiddenMobile">
                            <td height="40"></td>
                        </tr>
                        <tr class="visibleMobile">
                            <td height="30"></td>
                        </tr>

                        <tr>
                            <td>
                                <table width="650" border="0" cellpadding="0" cellspacing="0" align="center"
                                       class="fullPadding">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <table width="220" border="0" cellpadding="0" cellspacing="0" align="left"
                                                   class="col">
                                                <tbody>
                                                <tr>
                                                    <!--                                                    <td align="left"><img-->
                                                    <!--                                                                src="http://www.supah.it/dribbble/017/logo.png"-->
                                                    <!--                                                                width="32" height="32" alt="logo" border="0"/></td>-->
                                                </tr>
                                                <tr class="hiddenMobile">
                                                    <td height="40"></td>
                                                </tr>
                                                <tr class="visibleMobile">
                                                    <td height="20"></td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 14px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 18px; vertical-align: top; text-align: left;">
                                                        <?php $cId = $invoice['customer_id'];
                                                        $customerObj = new Customer_table_model();
                                                        $customer_info = $customerObj->get_customer_table($cId);
                                                        echo '<pre>' . $customer_info['full_name'] . '<br><b>' . $customer_info['company_name'] . '</b><br>' . $customer_info['address'] . '<br>' . $customer_info['contact_number'];

                                                        ?>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table width="220" border="0" cellpadding="0" cellspacing="0" align="right"
                                                   class="col">
                                                <tbody>
                                                <tr class="visibleMobile">
                                                    <td height="20"></td>
                                                </tr>
                                                <tr>
                                                    <td height="5"></td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 32px; color:#000; letter-spacing: -1px; font-family: 'Open Sans', sans-serif; line-height: 1; vertical-align: top; text-align: right;">
                                                        INVOICE
                                                    </td>
                                                </tr>
                                                <tr>

                                                <tr class="visibleMobile">
                                                    <td height="20"></td>
                                                </tr>

                                                <tr>
                                                    <td style="font-size: 14px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 18px; vertical-align: top; text-align: right;">
                                                        <small>INVOICE NO:</small>
                                                        <?php echo $invoice['invoice_no']; ?><br/>
                                                        <small>DATE: <?php echo $invoice['invoice_date']; ?></small>
                                                    </td>
                                                </tr>
                                                <tr class="hiddenMobile">
                                                    <td height="50"></td>
                                                </tr>


                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- /Header -->
        <!-- Order Details -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable"
               bgcolor="#e1e1e1">
            <tbody>
            <tr>
                <td>
                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable"
                           bgcolor="#ffffff">
                        <tbody>
                        <tr>
                        <tr class="hiddenMobile">
                            <td height="60"></td>
                        </tr>
                        <tr class="visibleMobile">
                            <td height="40"></td>
                        </tr>
                        <tr>
                            <td>
                                <table width="650" border="0" cellpadding="0" cellspacing="0" align="center"
                                       class="fullPadding">
                                    <tbody>
                                    <tr>
                                        <th style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 10px 7px 0;"
                                            width="32%" align="left">
                                            <strong>ITEM</strong>
                                        </th>
                                        <th style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;"
                                            align="left">
                                            <strong>PRICE</strong><sub>Tk/kg</sub>
                                        </th>
                                        <th class="text-center"
                                            style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;"
                                            align="center">
                                            <strong>QTY</strong><sub>kg</sub>
                                        </th>
                                        <th class="text-center"
                                            style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #5b5b5b; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;"
                                            align="center">
                                            <strong>Discount</strong><sub>Tk</sub>
                                        </th>
                                        <th style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #1e2b33; font-weight: normal; line-height: 1; vertical-align: top; padding: 0 0 7px;"
                                            class="text-right">
                                            <strong>TOTAL</strong><sub>Tk</sub>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td height="1" style="background: #bebebe;" colspan="5"></td>
                                    </tr>
                                    <tr>
                                        <td height="10" colspan="5"></td>
                                    </tr>
                                    <?php $sl = 1;
                                    foreach ($order_info as $o) { ?>
                                        <tr>
                                            <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #000;  line-height: 18px;  vertical-align: top; padding:10px 0;"
                                                class="article">
                                                <?php $pId = $o['product_id'];
                                                $proObj = new Product_table_model();
                                                $info = $proObj->get_product_table($pId);
                                                echo $info['product_name'];
                                                ?>
                                            </td>
                                            <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;">
                                                <?php echo $o['invoice_product_price']; ?>
                                            </td>
                                            <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;"
                                                align="center"><?php echo $o['invoice_product_qty']; ?><sub>Kg</sub>
                                            </td>
                                            <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top; padding:10px 0;"
                                                align="center"><?php echo $o['invoice_product_discount']; ?>
                                            </td>
                                            <td style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #1e2b33;  line-height: 18px;  vertical-align: top; padding:10px 0;"
                                                align="right"><?php echo $o['invoice_product_sub']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" colspan="5" style="border-bottom:1px solid #e4e4e4"></td>
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="20"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>


        <!-- /Order Details -->
        <!-- Total -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable"
               bgcolor="#e1e1e1">
            <tbody>
            <tr>
                <td>
                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable"
                           bgcolor="#ffffff">
                        <tbody>
                        <tr>
                            <td>

                                <!-- Table Total -->
                                <table width="650" border="0" cellpadding="0" cellspacing="0" align="center"
                                       class="fullPadding">
                                    <tbody>
                                    <tr>
                                        <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; ">
                                            Subtotal <sub>Tk</sub>&nbsp;
                                        </td>
                                        <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; white-space:nowrap;"
                                            width="80">
                                            <?php echo $invoice['subTotal']; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; ">
                                            Discount <sub>Tk</sub>&nbsp;
                                        </td>
                                        <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #646a6e; line-height: 22px; vertical-align: top; text-align:right; ">
                                            <?php echo $invoice['discount']; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right; ">
                                            <strong>Paid</strong> <sub>Tk</sub>&nbsp;
                                        </td>
                                        <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right; ">
                                            <strong id=""><?php echo $invoice['paidAmount']; ?></strong>
                                            <input type="hidden" id="paid_amount"
                                                   value="<?php echo $invoice['paidAmount'] ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #b0b0b0; line-height: 22px; vertical-align: top; text-align:right; ">
                                            <strong>Due</strong> <sub>Tk</sub>&nbsp;
                                        </td>
                                        <td style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #b0b0b0; line-height: 22px; vertical-align: top; text-align:right; ">
                                            <strong><?php echo $invoice['due']; ?></strong>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- /Table Total -->

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- /Total -->
        <!-- Information -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable"
               bgcolor="#e1e1e1">
            <tbody>
            <tr>
                <td>

                </td>
            </tr>
            </tbody>
        </table>
        <!-- /Information -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable"
               bgcolor="#e1e1e1">

            <tr>
                <td>
                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="fullTable"
                           bgcolor="#ffffff" style="border-radius: 0 0 10px 10px;">
                        <tr>
                            <td>
                                <table width="650" border="0" cellpadding="0" cellspacing="0" align="center"
                                       class="fullPadding">
                                    <tbody>
                                    <tr>
                                        <td style="font-size: 12px; color: #5b5b5b; font-family: 'Open Sans', sans-serif; line-height: 18px; vertical-align: top; text-align: left;">

                                            In words:<span id="inword"
                                                           style="font-size: 12px; margin: 0px 0px 0px 0px">   </span><br>
                                            Remarks: <?php echo $invoice['remarks']; ?><br>
                                            Receiver: <?php echo $this->session->userdata('full_name'); ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="spacer">
                            <td height="50"></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
        </table>
    </div>
    <!--        <input type="button" id="printButton" style="margin-right: 20%;margin-bottom: 20% " class="btn btn-info pull-right"-->
    <!--               onclick="printDiv('print2')" value="print"/>-->
    <button id="printButton" style="margin-right: 20%;margin-bottom: 20% " class="btn btn-info pull-right"
            onclick="printDiv('print2')"><i class="fa fa-print" aria-hidden="true"></i> print
    </button>


    <script>
        $(function () {
            $('#Table').DataTable({

                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        });
    </script>


    <table id="Table" class="table table-bordered table-striped">
        <caption>Payment History</caption>
        <style>
            .table td {
                text-align: center;
            }

            caption {
                font-size: 32px;
                font-weight: bold;
                margin-left: 20px;

            }


        </style>

        <thead>
        <tr>

            <th class="text-center">Invoice No</th>
            <th class="text-center">Customer Name</th>
            <th class="text-center">Payment Date</th>
            <th class="text-center">Paid Amount</th>

        </tr>
        </thead>
        <?php foreach ($payment_table as $p) { ?>
            <tr>

                <td><?php echo $p['invoice_no']; ?></td>
                <td>
                    <?php $id = $p['customer_id'];
                    $customerObj = new Customer_table_model();
                    $info = $customerObj->get_customer_table($id);
                    echo $info['full_name'] . '<br><b>' . $info['company_name'] . '</b>';


                    ?>
                </td>
                <td><?php echo $p['invoice_date']; ?></td>
                <td><?php echo $p['total_amount']; ?></td>

            </tr>
        <?php } ?>
    </table>


</div>


<!-- /.content-wrapper -->
<script src="<?php echo base_url() ?>js/numtoword.js"></script>
<script>
    var numtoword = NumToWord($("#paid_amount").val(), 'in_word');
    $("#inword").text(numtoword + 'Taka Only');

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

</script>
