<main class="content">
    <?php echo form_open('invoice_controller/edit/' . $invoice['id']); ?>
    <div class="content_inner">
        <?php echo validation_errors(); ?>


        <div>Status : <input type="text" name="status"
                             value="<?php echo($this->input->post('status') ? $this->input->post('status') : $invoice['status']); ?>"/>
        </div>
        <div>User : <input type="text" name="user"
                           value="<?php echo($this->input->post('user') ? $this->input->post('user') : $invoice['user']); ?>"/>
        </div>
        <div>Invoice No : <input type="text" name="invoice_no"
                                 value="<?php echo($this->input->post('invoice_no') ? $this->input->post('invoice_no') : $invoice['invoice_no']); ?>"/>
        </div>
        <div>Invoice Date : <input type="text" name="invoice_date"
                                   value="<?php echo($this->input->post('invoice_date') ? $this->input->post('invoice_date') : $invoice['invoice_date']); ?>"/>
        </div>
        <div>SubTotal : <input type="text" name="subTotal"
                               value="<?php echo($this->input->post('subTotal') ? $this->input->post('subTotal') : $invoice['subTotal']); ?>"/>
        </div>
        <div>PaidAmount : <input type="text" name="paidAmount"
                                 value="<?php echo($this->input->post('paidAmount') ? $this->input->post('paidAmount') : $invoice['paidAmount']); ?>"/>
        </div>
        <div>Due : <input type="text" name="due"
                          value="<?php echo($this->input->post('due') ? $this->input->post('due') : $invoice['due']); ?>"/>
        </div>
        <div>Discount : <input type="text" name="discount"
                               value="<?php echo($this->input->post('discount') ? $this->input->post('discount') : $invoice['discount']); ?>"/>
        </div>
        <div>Remarks : <textarea
                    name="remarks"><?php echo($this->input->post('remarks') ? $this->input->post('remarks') : $invoice['remarks']); ?></textarea>
        </div>
        <div>Billed To : <textarea
                    name="billed_to"><?php echo($this->input->post('billed_to') ? $this->input->post('billed_to') : $invoice['billed_to']); ?></textarea>
        </div>

        <button type="submit">Save</button>

        <?php echo form_close(); ?>

    </div>
</main>
