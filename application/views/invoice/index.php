<link rel="stylesheet" type="text/css"
      href="<?php echo base_url() ?>plugins/datetimepicker/jquery.datetimepicker.min.css"/>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/select2/select2.min.css">
<script src="<?php echo base_url() ?>js\tinymce\js\tinymce\tinymce.min.js"></script>
<style>
    .table td {
        text-align: center;
    }

    .float-left {
        float: left !important;
    }

    .float-right {
        float: right !important;
    }

    .margin-bottom {
        margin-bottom: 1em;
    }

    .margin-top {
        margin-top: 1em;
    }

    .margin-left {
        margin-left: 1em;
    }

    /* Form validation */
    #response {
        margin: 0 1em 1em 1em;
    }

    .paddingBottom {
        padding-bottom: 100px;
    }


</style>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chalan List
            <small>All chalan</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>

                                <th class="text-center"> Chalan #</th>
                                <th class="text-center"> Date</th>
                                <th class="text-center">Customer</th>
                                <th class="text-center">SubTotal</th>
                                <th class="text-center">PaidAmount</th>
                                <th class="text-center">Due</th>
                                <th class="text-center">Discount</th>
                                <th class="text-center">Remarks</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Reciever</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <?php foreach ($invoice as $i) { ?>
                                <tr>

                                    <td><?php echo $i['invoice_no']; ?></td>
                                    <td><?php echo $i['invoice_date']; ?></td>
                                    <td>
                                        <?php $id = $i['customer_id'];
                                        $customerObj = new Customer_table_model();
                                        $info = $customerObj->get_customer_table($id);
                                        echo $info['full_name'] . '<br><b>' . $info['company_name'] . '</b>';


                                        ?>
                                    </td>
                                    <td><?php echo $i['subTotal']; ?></td>
                                    <td><?php echo $i['paidAmount']; ?></td>
                                    <td><?php echo $i['due']; ?></td>
                                    <td><?php echo $i['discount']; ?></td>
                                    <td><?php echo $i['remarks']; ?></td>
                                    <td><?php $status = $i['status'];
                                        if ($status == 0) {
                                            echo "<span class='text-red'><b>Due</b></span>";
                                        } else {
                                            echo "<span class='text-green'><b>Paid</b></span>";
                                        }
                                        ?></td>
                                    <td><?php $userId = $i['user'];
                                        $userObj = new User_table_model();
                                        $userName = $userObj->get_user_table($userId);
                                        echo $userName['full_name'];
                                        ?></td>

                                    <td>
                                        <a href="<?php echo site_url('invoice_controller/viewInvoiceById/' . $i['invoice_no']); ?>"
                                           class="btn btn-xs btn-warning"><i class="fa fa-eye"></i></a>
                                        <!--                <a href="-->
                                        <?php //echo site_url('invoice_controller/edit/'.$i['invoice_no']); ?><!--" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>-->

                                        <?php $status = $i['status'];
                                        if ($status == 0)
                                        { ?>
                                        <a href="#" data-toggle="modal" data-id="<?php echo $i['invoice_no']; ?>"
                                           data-target="#myModal" class="btn btn-xs btn-danger modalopen"><i
                                                    class="fa fa-plus"></i></a>
                                    </td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>


                    <?php echo form_open('payment_table_controller/add'); ?>
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Add Payment</h4>

                                </div>
                                <div class="modal-body">

                                    <table id="add">
                                        <tr>
                                            <th width="40%">Invoice No:</th>

                                            <th>
                                                <div class="input-group col-xs-10 float-right margin-bottom">
                                                    <span class="input-group-addon">#</span>
                                                    <input type="text" class="form-control invoice_no" name="invoice_no"
                                                           id="invoice_no" readonly>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Date</th>
                                            <th>
                                                <div class="input-group col-xs-10 float-right margin-bottom " id="date">
                                                    <span class="input-group-addon"><i
                                                                class="fa fa-calendar"></i></span>
                                                    <input type="text" id="invoiceDate" name="invoice_date"
                                                           class="form-control" placeholder=""
                                                           aria-describedby="sizing-addon1" required>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Sub Total</th>
                                            <th>
                                                <div class="input-group col-xs-10 float-right margin-bottom">

                                                    <input type="text" id="subtotal" name="subtotal"
                                                           class="form-control"
                                                           aria-describedby="sizing-addon1" readonly>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Discount</th>
                                            <th>
                                                <div class="input-group col-xs-10 float-right margin-bottom">

                                                    <input type="text" id="discount" name="discount"
                                                           class="form-control"
                                                           aria-describedby="sizing-addon1" readonly>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Paid</th>
                                            <th>
                                                <div class="input-group col-xs-10 float-right margin-bottom">

                                                    <input type="text" id="paidAmount" name="paidAmount"
                                                           class="form-control"
                                                           aria-describedby="sizing-addon1" readonly>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Due</th>
                                            <th>
                                                <div class="input-group col-xs-10 float-right margin-bottom">

                                                    <input type="text" id="due" name="due" class="form-control"
                                                           aria-describedby="sizing-addon1" readonly>
                                                </div>
                                            </th>
                                        </tr>

                                        <tr>
                                            <th>Current Payment</th>
                                            <th>
                                                <div class="input-group col-xs-10 float-right margin-bottom">

                                                    <input type="text" id="currentPayment" name="currentPayment"
                                                           class="form-control"
                                                           aria-describedby="sizing-addon1" required>
                                                </div>
                                            </th>
                                        </tr>

                                        <tr>
                                            <th>Remarks</th>
                                            <th>
                                                <div class="input-group col-xs-10 float-right margin-bottom">

                                                    <textarea class="form-control" id="remarks" placeholder="remarks..."
                                                              name="remarks"></textarea>
                                                    <input type="hidden" name="customer_id" id="customer_id">
                                                </div>
                                            </th>
                                        </tr>

                                    </table>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>


                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </section>
</div>


<script>

    $(document).on("click", ".modalopen", function () {
        var InvoiceNo = $(this).data('id');
        $(".modal-body #invoice_no").val(InvoiceNo);
        var url = '<?php echo site_url("Invoice_controller/get_invoice_data_by_invoice_id");?>';
        $.ajax({
            type: "POST",
            url: url,
            data: {'invoice_no': InvoiceNo},
            success: function (data) {
                console.log(data);
                if (data == 'fail') {
                    alert("there is no data");
                }
                else {

                    var json = $.parseJSON(data);

                    $("#subtotal").val(json.subTotal);
                    $("#discount").val(json.discount);
                    $("#paidAmount").val(json.paidAmount);
                    $("#due").val(json.due);
                    $("#customer_id").val(json.customer_id);

                }
            }
        });
    });

    //
    //    $('#myModal').on('show.bs.modal', function (e) {
    //
    //        var InvoiceNo = $(e.relatedTarget).attr('data-id');
    //
    //        $(this).find('#invoice_no').val(InvoiceNo);
    //
    //
    //
    //    });

    $('#invoiceDate').datetimepicker({
        format: 'Y-m-d',
        step: 30,

    });
    //    $(document).ready(function () {
    //        $('#date').datetimepicker({
    //            defaultDate: new Date()
    //        });
    //    });


    $(document).ready(function () {
        //this calculates values automatically
        sum();


        $("#currentPayment").change(function () {

//            console.log(paidAmount);
//            console.log(": here :");
//            console.log(dueAmount);
            sum();
        });

        function sum() {
            var currentPayment = $('#currentPayment').val();
            var paidAmount = $('#paidAmount').val();
            var dueAmount = $('#due').val();


            if (!isNaN(currentPayment)) {
                var totalResult = parseInt(currentPayment) + parseInt(paidAmount);
                var dueResult = parseInt(dueAmount) - parseInt(currentPayment);

                if (!isNaN(totalResult)) {

                    $("#paidAmount").val(totalResult);
                    $("#due").val(dueResult);

                }
            }

        }

    });
</script>

