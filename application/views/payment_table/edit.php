<?php echo validation_errors(); ?>

<?php echo form_open('payment_table_controller/edit/' . $payment_table['id']); ?>

    <div>Invoice No : <input type="text" name="invoice_no"
                             value="<?php echo($this->input->post('invoice_no') ? $this->input->post('invoice_no') : $payment_table['invoice_no']); ?>"/>
    </div>
    <div>Invoice Date : <input type="text" name="invoice_date"
                               value="<?php echo($this->input->post('invoice_date') ? $this->input->post('invoice_date') : $payment_table['invoice_date']); ?>"/>
    </div>
    <div>Total Amount : <input type="text" name="total_amount"
                               value="<?php echo($this->input->post('total_amount') ? $this->input->post('total_amount') : $payment_table['total_amount']); ?>"/>
    </div>
    <div>User : <input type="text" name="user"
                       value="<?php echo($this->input->post('user') ? $this->input->post('user') : $payment_table['user']); ?>"/>
    </div>

    <button type="submit">Save</button>

<?php echo form_close(); ?>