!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Inventory
            <small>All Products</small>
        </h1>

    </section>

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>
                        <table id="userTable" class="table table-bordered table-striped">
                            <style>
                                .table td {
                                    text-align: center;
                                }


                            </style>

                            <thead>
                            <tr>

                                <th class="text-center">Invoice No</th>
                                <th class="text-center">Customer Name</th>
                                <th class="text-center">Invoice Date</th>
                                <th class="text-center">Total Amount</th>
                                <!--		<th>User</th>-->
                                <!--		<th>Actions</th>-->
                            </tr>
                            </thead>
                            <?php foreach ($payment_table as $p) { ?>
                                <tr>

                                    <td><?php echo $p['invoice_no']; ?></td>
                                    <td>
                                        <?php $id = $p['customer_id'];
                                        $customerObj = new Customer_table_model();
                                        $info = $customerObj->get_customer_table($id);
                                        echo $info['full_name'] . '<br><b>' . $info['company_name'] . '</b>';


                                        ?>
                                    </td>
                                    <td><?php echo $p['invoice_date']; ?></td>
                                    <td><?php echo $p['total_amount']; ?></td>
                                    <!--		<td>--><?php //echo $p['user']; ?><!--</td>-->
                                    <!--		<td>-->
                                    <!--            <a href="-->
                                    <?php //echo site_url('payment_table_controller/edit/'.$p['id']); ?><!--">Edit</a> | -->
                                    <!--            <a href="-->
                                    <?php //echo site_url('payment_table_controller/remove/'.$p['id']); ?><!--">Delete</a>-->
                                    <!--        </td>-->
                                </tr>
                            <?php } ?>
                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->