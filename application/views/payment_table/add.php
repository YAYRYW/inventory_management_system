<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>js/datetimepicker/jquery.datetimepicker.min.css"/>
<script src="<?php echo base_url() ?>js/datetimepicker/jquery.datetimepicker.full.js"></script>
<style>
    .marginTop {
        margin-top: 1em;
    }
</style>

<main class="content">
    <div class="content_inner">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <?php $this->load->view('/flashMessage'); ?>


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " id="print">
                                <link href="<?php echo base_url() ?>plugins/css/money_receipt.css" rel="stylesheet">

                                <div class="receiptBody col-lg-9 col-md-9 col-sm-9 col-xs-9 ">
                                    <div class="receipthead">
                                        <!--                                    <div class="Cname">-->
                                        <!--                                        <p class="nameHeader">Company Name</small></p>-->

                                        <!--                                    </div>-->
                                        <div class="logo"><img src="<?php echo base_url('img/Inviitlogo2.jpg') ?>"
                                                               height="100% "
                                                               width="100%"></div>


                                    </div>
                                    <div class="receiptSection">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  no-padding  ">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 marginTop  no-padding">

                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 marginTop   no-padding">
                                                <span style="font-size: 13px; margin: 5px 0px 0px 0px"> <strong>MONEY RECEIPT</strong></span>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 marginTop  no-padding ">


                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  no-padding  ">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 marginTop  text-left no-padding">
                                                <span style="font-size: 13px; margin: 5px 0px 0px 0px"> <strong>Invoice No: <?php echo $invoice['invoice_no']; ?></strong></span>

                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 marginTop   no-padding">

                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 marginTop  no-padding ">


                                            </div>
                                        </div>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  no-padding  ">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6   no-padding">
                                                <span style="font-size: 13px; margin: 5px 0px 0px 0px;text-transform: uppercase;"> <strong>Name:</strong> <?php echo $invoice['billed_to']; ?></span>

                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right   no-padding">
                                                <span style="font-size: 12px; margin: 5px 0px 0px 0px">   <strong>Date: <?php echo $invoice['invoice_date']; ?></strong></span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3    no-padding">
                                                <!--                                            <span style="font-size: 12px; text-align:right; margin: 5px 0px 0px 0px;text-align:">   <strong>(ORIGINAL COPY)</strong></span>-->
                                            </div>


                                            <!--                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4  marginTop no-paddingd">-->
                                            <!--                                            Age: --><?php //echo $patient_info['patient_age']; ?>
                                            <!---->
                                            <!--                                        </div>-->
                                            <!--                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4  marginTop  no-paddingd">-->
                                            <!--                                            Sex: --><?php //echo $patient_info['patient_gender']; ?>
                                            <!---->
                                            <!--                                        </div>-->
                                            <!--                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4  marginTop  no-padding">-->
                                            <!--                                            Mobile: --><?php //echo $patient_info['patient_mobile_number']; ?>
                                            <!--                                        </div>-->
                                            <!--                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  marginTop marginBottom   no-padding">-->
                                            <!--                                            Ref by: --><?php //echo $patient_info['patient_reffered_by']; ?>
                                            <!--                                        </div>-->


                                            <table class=" table table-bordered table-striped marginTop">

                                                <thead>
                                                <tr>
                                                    <th>
                                                        Sl No
                                                    </th>

                                                    <th>
                                                        <small>Product</small>
                                                    </th>
                                                    <th>
                                                        <small>Qty</small>
                                                    </th>

                                                    <th class="text-right">
                                                        <small>Unit Price(Tk.)</small>
                                                    </th>
                                                    <th>
                                                        <small>Discount</small>
                                                    </th>
                                                    <th>
                                                        <small>Total</small>
                                                    </th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $sl = 1;
                                                foreach ($order_info as $o) { ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $sl++; ?>
                                                        </td>
                                                        <td>
                                                            <small><?php echo $o['product']; ?></small>
                                                        </td>
                                                        <td>
                                                            <small><?php echo $o['invoice_product_qty']; ?>
                                                            </small>
                                                        </td>

                                                        <td class="text-right">
                                                            <small><?php echo $o['invoice_product_price']; ?></small>
                                                        </td>
                                                        <td class="text-right">
                                                            <small><?php echo $o['invoice_product_discount']; ?></small>
                                                        </td>
                                                        <td class="text-right">
                                                            <small><?php echo $o['invoice_product_sub']; ?></small>
                                                        </td>
                                                    </tr>
                                                <?php } ?>

                                                <tr>
                                                    <td>Remarks:</td>
                                                    <td>
                                                        <small><?php echo $invoice['remarks']; ?></small>
                                                    </td>

                                                </tr>

                                                </tbody>
                                            </table>

                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7   no-padding">
                                                </strong><br>
                                                <?php if ($invoice['due'] == 0) { ?>

                                                    <img src="<?php echo base_url() ?>/images/paid.png"
                                                         width="100px" height="100px">
                                                <?php } ?>

                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5      no-padding">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <td class="">
                                                            <small>Sub Total Tk.</small>
                                                        </td>
                                                        <td class="text-right">
                                                            <small><?php echo $invoice['subTotal']; ?></small>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="">
                                                            <small>Discount Tk.</small>
                                                        </td>
                                                        <td class="text-right">
                                                            <small><?php echo $invoice['discount']; ?></small>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="">
                                                            <small>Paid Tk.</small>
                                                        </td>
                                                        <td class="text-right">
                                                            <small><?php echo $invoice['paidAmount']; ?></small>
                                                            <input type="hidden" id="paid_amount"
                                                                   value="<?php echo $invoice['paidAmount']; ?>"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class=""><strong>
                                                                <small>Due Tk.</small>
                                                            </strong></td>
                                                        <td class="text-right ">
                                                            <small><?php echo $invoice['due']; ?></small>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </div>

                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7   no-padding">
                                                In Words:<span id="inword"
                                                               style="font-size: 12px; margin: 0px 0px 0px 0px">   </span><br>
                                                Receiver: <?php echo $this->session->userdata('full_name'); ?>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5  no-padding">
                                            <span style="font-size: 10px; float: right">Developed By: <br><img
                                                        src="<?php echo base_url() ?>/images/img/Inviitlogo2.jpg"
                                                        width="100px" height="25px"></span>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  no-padding">
                                                <span style="font-size: 10px; margin: 0px 0px 0px 0px; text-align: center">       </span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 "></div>
                            </div>


                            <?php echo form_open('payment_table_controller/add'); ?>


                            <div> Date : <input type="text" id="invoiceDate" name="invoice_date"
                                                value="<?php echo $this->input->post('invoice_date'); ?>"/></div>
                            <div>Total Amount : <input type="text" name="total_amount"
                                                       value="<?php echo $this->input->post('total_amount'); ?>"/></div>


                            <button type="submit">Save</button>

                            <?php echo form_close(); ?>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="<?php echo base_url() ?>js/numtoword.js"></script>
    <script>
        var numtoword = NumToWord($("#paid_amount").val(), 'in_word');
        $("#inword").text(numtoword + 'Taka Only');

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }

        $('#invoiceDate').datetimepicker({
            format: 'd-m-Y g:i A',
            step: 30
        });
    </script>

