<table border="1" width="100%">
    <tr>
        <th>ID</th>
        <th>Invoice No</th>
        <th>Qty</th>
        <th>Rate</th>
        <th>Total Amount</th>
        <th>Item</th>
        <th>Actions</th>
    </tr>
    <?php foreach ($order_table as $o) { ?>
        <tr>
            <td><?php echo $o['id']; ?></td>
            <td><?php echo $o['invoice_no']; ?></td>
            <td><?php echo $o['qty']; ?></td>
            <td><?php echo $o['rate']; ?></td>
            <td><?php echo $o['total_amount']; ?></td>
            <td><?php echo $o['item']; ?></td>
            <td>
                <a href="<?php echo site_url('order_table_controller/edit/' . $o['id']); ?>">Edit</a> |
                <a href="<?php echo site_url('order_table_controller/remove/' . $o['id']); ?>">Delete</a>
            </td>
        </tr>
    <?php } ?>
</table>