<?php echo validation_errors(); ?>

<?php echo form_open('order_table_controller/edit/' . $order_table['id']); ?>

    <div>Invoice No : <input type="text" name="invoice_no"
                             value="<?php echo($this->input->post('invoice_no') ? $this->input->post('invoice_no') : $order_table['invoice_no']); ?>"/>
    </div>
    <div>Qty : <input type="text" name="qty"
                      value="<?php echo($this->input->post('qty') ? $this->input->post('qty') : $order_table['qty']); ?>"/>
    </div>
    <div>Rate : <input type="text" name="rate"
                       value="<?php echo($this->input->post('rate') ? $this->input->post('rate') : $order_table['rate']); ?>"/>
    </div>
    <div>Total Amount : <input type="text" name="total_amount"
                               value="<?php echo($this->input->post('total_amount') ? $this->input->post('total_amount') : $order_table['total_amount']); ?>"/>
    </div>
    <div>Item : <textarea
                name="item"><?php echo($this->input->post('item') ? $this->input->post('item') : $order_table['item']); ?></textarea>
    </div>

    <button type="submit">Save</button>

<?php echo form_close(); ?>