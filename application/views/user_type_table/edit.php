<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Edit</h3>
            </div>
            <?php echo validation_errors(); ?>
            <?php echo form_open('user_type_controller/edit/' . $user_type_table['id'], array("class" => "form-horizontal")); ?>

            <div class="box-body">
                <div class="form-group">
                    <label for="user_type" class="col-md-4 control-label">User Type</label>
                    <div class="col-md-8">
                        <input type="text" name="user_type"
                               value="<?php echo($this->input->post('user_type') ? $this->input->post('user_type') : $user_type_table['user_type']); ?>"
                               class="form-control" id="user_type"/>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <div class="form-group">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-check"></i> Save
                        </button>
                    </div>
                </div>
            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
</div>