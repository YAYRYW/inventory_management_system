<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product List
            <small>All products</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>

                                <th>Product Name</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($product_table as $p) { ?>
                            <tr>
                                <td><?php echo $p['product_id']; ?></td>
                                <td><?php echo $p['product_name']; ?></td>


                                <td>
                                    <a href="<?php echo site_url('product_controller/edit/' . $p['product_id']); ?>">Edit</a>
                                    |
                                    <a href="<?php echo site_url('product_controller/remove/' . $p['product_id']); ?>">Delete</a>
                                </td>
                            </tr>
                            </tbody>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>