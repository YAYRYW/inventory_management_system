<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product
            <small>Add a new product</small>
        </h1>
        <?php $this->load->view('/flashMessage'); ?>
    </section>
    <section class="content">

        <div class="register-box">
            <div class="register-box-body">

                <?php echo form_open('product_controller/edit/' . $product_table['product_id'], ['id' => 'add']); ?>


                <div class="form-group has-feedback">
                    <label>Product Name </label>
                    <input type="text" class="form-control" name="product_name"
                           value="<?php echo($this->input->post('product_name') ? $this->input->post('product_name') : $product_table['product_name']); ?>"
                           required/>

                </div>

                <?php echo validation_errors(); ?>
                <button type="submit">Save</button>

                <?php echo form_close(); ?>

            </div>
        </div>
    </section>
</div>

<script>
    $("#add").validate({});