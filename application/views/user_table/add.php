<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Register
            <small>Register a new user</small>
        </h1>
        <?php $this->load->view('/flashMessage'); ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="register-box">
            <div class="register-box-body">
                <?php echo form_open('User_controller/add', ['id' => 'add_user', 'class' => '']); ?>

                <div class="form-group has-feedback">
                    <label>Fullname</label>
                    <input type="text" name="full_name" value="<?php echo $this->input->post('full_name'); ?>"
                           class="form-control" id="full_name" required/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label>Address</label>
                    <input type="text" name="address" value="<?php echo $this->input->post('address'); ?>"
                           class="form-control" id="address"/>
                    <span class="glyphicon glyphicon-home form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label>Contact No</label>
                    <input type="text" name="mobile_no" value="<?php echo $this->input->post('mobile_no'); ?>"
                           class="form-control" id="mobile_no" required/>
                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label>Username</label>
                    <input type="text" name="user_name" value="<?php echo $this->input->post('user_name'); ?>"
                           class="form-control" id="user_name"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <select name="user_type_id" class="form-control" required>

                        <option value="">select</option>
                        <?php
                        foreach ($all_user_type_table as $user_type_table) {
                            $selected = ($user_type_table['id'] == $this->input->post('user_type_id')) ? ' selected="selected"' : "";

                            echo '<option value="' . $user_type_table['id'] . '" ' . $selected . '>' . $user_type_table['user_type'] . '</option>';
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group has-feedback">
                    <label>Password</label>
                    <input type="password" name="password" value="<?php echo $this->input->post('password'); ?>"
                           class="form-control" id="password" required/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Retype password" name="password_confirm"
                           data-rule-equalTo="#new_password" required>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">

                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <span class="error"><?php echo validation_errors(); ?></span>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                    </div>
                    <!-- /.col -->
                </div>

                <?php echo form_close(); ?>
            </div>
            <!-- /.form-box -->
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    $("#add_user").validate({
        rules: {

            password: {
                minlength: 6
            },
            password_confirm: {
                minlength: 6,
                equalTo: '[name="password"]'
            }
        },


    });
</script>


