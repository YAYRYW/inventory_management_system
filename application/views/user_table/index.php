!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
            <small>All Users</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>
                        <table id="userTable" class="table table-bordered table-striped">

                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Contact No</th>
                                <th>User Name</th>
                                <th>Address</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($user_table as $u) { ?>
                                <tr>
                                    <td><?php echo $u['user_id']; ?></td>
                                    <td><?php echo $u['full_name']; ?></td>
                                    <td><?php echo $u['mobile_no']; ?></td>
                                    <td><?php echo $u['user_name']; ?></td>
                                    <td><?php echo $u['address']; ?></td>

                                    <td><?php $type = $u['user_type_id'];
                                        $usrType = new User_type_table_model();
                                        $typename = $usrType->get_user_type_table($type);
                                        echo $typename['user_type'];

                                        ?></td>

                                    <td>
                                        <a href="<?php echo site_url('user_controller/edit/' . $u['user_id']); ?>"
                                           class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>
                                        <a href="<?php echo site_url('user_controller/remove/' . $u['user_id']); ?>"
                                           class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
