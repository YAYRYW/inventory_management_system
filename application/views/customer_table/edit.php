<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer
            <small>edit customer</small>
        </h1>
        <?php $this->load->view('/flashMessage'); ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="register-box">
            <div class="register-box-body">
                <?php echo form_open('customer_table_controller/edit/' . $customer_table['id'], array("class" => "form-horizontal")); ?>

                <div class="form-group">
                    <label for="company_name" class="col-md-4 control-label">Company Name</label>
                    <div class="col-md-8">
                        <input type="text" name="company_name"
                               value="<?php echo($this->input->post('company_name') ? $this->input->post('company_name') : $customer_table['company_name']); ?>"
                               class="form-control" id="company_name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="full_name" class="col-md-4 control-label">Full Name</label>
                    <div class="col-md-8">
                        <input type="text" name="full_name"
                               value="<?php echo($this->input->post('full_name') ? $this->input->post('full_name') : $customer_table['full_name']); ?>"
                               class="form-control" id="full_name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-md-4 control-label">Address</label>
                    <div class="col-md-8">
                        <input type="text" name="address"
                               value="<?php echo($this->input->post('address') ? $this->input->post('address') : $customer_table['address']); ?>"
                               class="form-control" id="address"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="contact_number" class="col-md-4 control-label">Contact Number</label>
                    <div class="col-md-8">
                        <input type="text" name="contact_number"
                               value="<?php echo($this->input->post('contact_number') ? $this->input->post('contact_number') : $customer_table['contact_number']); ?>"
                               class="form-control" id="contact_number"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>

                <?php echo form_close(); ?>

            </div>
            <!-- /.form-box -->
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    $("#add_user").validate({
        rules: {

            password: {
                minlength: 6
            },
            password_confirm: {
                minlength: 6,
                equalTo: '[name="password"]'
            }
        },


    });
</script>

