!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer
            <small>All customers</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>
                        <table id="userTable" class="table table-bordered table-striped">

                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company Name</th>
                                <th>Full Name</th>
                                <th>Address</th>
                                <th>Contact Number</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <?php foreach ($customer_table as $c) { ?>
                                <tr>
                                    <td><?php echo $c['id']; ?></td>
                                    <td><?php echo $c['company_name']; ?></td>
                                    <td><?php echo $c['full_name']; ?></td>
                                    <td><?php echo $c['address']; ?></td>
                                    <td><?php echo $c['contact_number']; ?></td>
                                    <td>
                                        <a href="<?php echo site_url('customer_table_controller/edit/' . $c['id']); ?>"
                                           class="btn btn-info btn-xs">Edit</a>
                                        <a href="<?php echo site_url('customer_table_controller/remove/' . $c['id']); ?>"
                                           class="btn btn-danger btn-xs">Delete</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
