<script src="<?php echo base_url() ?>plugins/chartjs/Chart.min.js"></script>
!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Inventory
            <small>Available products</small>
        </h1>

    </section>

    <style>
        article:nth-child(4n+4) .progress-bar {
            background: #cf5bdd;
        }

        article:nth-child(4n+1) .progress-bar {
            background: green;
        }

        article:nth-child(4n+2) .progress-bar {
            background: orange;
        }

        article:nth-child(4n+3) .progress-bar {
            background: #00acd6;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>
                        <?php foreach ($inventory_table as $i) { ?>

                            <article class="col-lg-6 col-md-6 col-sm-6 ">

                                <div class="progress-group">
                                            <span class="progress-text"><?php $productId = $i['product_id'];
                                                $proObj = new Product_table_model();
                                                $name = $proObj->get_product_table($productId);
                                                echo $name['product_name'];
                                                ?></span>
                                    <span class="progress-number col-lg-6 col-md-6 col-sm-6"><b></b><?php
                                        $productId = $i['product_id'];
                                        $invObj = new Product_service();
                                        $soldProqty = $invObj->getSoldReturnedDamageProductAmount($productId);
                                        $proQty = $invObj->getTotalProductQuantityByProductId($productId);

                                        $sold = $soldProqty['TotalSold'];
                                        $Qty = $proQty['totalProduct'];


                                        if ($sold == null) {
                                            $sold = "00";
                                        }

                                        echo "Sold: " . $sold . "<sub>kg</sub> &nbsp;/  Total: " . $Qty . "<sub>kg</sub>";
                                        $percentageValue = $invObj->Getpercentage($sold, $Qty);

                                        ?>

                                        <div class="progress sm" style="width: 100%">
                                                <div class="progress-bar"
                                                     style="width: <?php echo $percentageValue; ?>"></div>

                                            </div>


                                </div>

                            </article>


                            <!--                                    <canvas id="pieChart" style="height:250px"></canvas>-->


                        <?php } ?>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <div class="box">
                    <div class="box-body">
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>Total</th>
                                <th>Sold</th>

                                <th>Available</th>
                                <!--		<th>Actions</th>-->
                            </tr>
                            </thead>

                            <?php foreach ($inventory_table as $i) { ?>
                                <tr>
                                    <td><?php echo $i['product_id']; ?></td>
                                    <td><?php $productId = $i['product_id'];
                                        $proObj = new Product_table_model();
                                        $name = $proObj->get_product_table($productId);
                                        echo $name['product_name'];
                                        ?></td>
                                    <td><?php
                                        $productId = $i['product_id'];
                                        $invObj = new Product_service();
                                        $proQty = $invObj->getTotalProductQuantityByProductId($productId);
                                        $Qty = $proQty['totalProduct'];

                                        echo $Qty . "&nbsp;<sub>kg</sub>";

                                        ?></td>
                                    <td><?php
                                        $productId = $i['product_id'];
                                        $invObj = new Product_service();
                                        $soldProqty = $invObj->getSoldReturnedDamageProductAmount($productId);
                                        $sold = $soldProqty['TotalSold'];
                                        if ($sold == 0) {
                                            $sold = 0;
                                        }

                                        echo $sold . "&nbsp;<sub>Kg</sub>";

                                        ?></td>

                                    <td>
                                        <?php

                                        $total = ($Qty - $sold);

                                        echo $total . "&nbsp;<sub>kg</sub>";
                                        ?>
                                    </td>

                                </tr>

                            <?php } ?>
                        </table>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



