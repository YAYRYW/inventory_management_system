!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Inventory
            <small>All Products</small>
        </h1>

    </section>

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product Id</th>
                                <th>Product Quantity</th>
                                <th>Purchase Rate <sub>per kg</sub></th>
                                <th>Purchase Date</th>
                                <th>Purchase Total Amount</th>
                                <th>Login User Id</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <?php foreach ($inventory_table as $i) { ?>
                                <tr>
                                    <td><?php echo $i['id']; ?></td>
                                    <td><?php $proId = $i['product_id'];
                                        $proObj = new Product_table_model();
                                        $pname = $proObj->get_product_table($proId);
                                        echo $pname['product_name'];
                                        ?></td>
                                    <td><?php echo $i['product_quantity']; ?> &nbsp; <sub>kg</sub></td>
                                    <td><?php echo $i['purchase_rate']; ?> &nbsp; <sub>per kg</sub></td>
                                    <td><?php echo $i['purchase_date']; ?></td>
                                    <td><?php echo $i['purchase_total_amount']; ?> &nbsp; <sub>Tk</sub></td>
                                    <td><?php $uId = $i['login_user_id'];
                                        $userObj = new User_table_model();
                                        $name = $userObj->get_user_table($uId);
                                        echo $name['full_name'];
                                        ?></td>
                                    <td>
                                        <a href="<?php echo site_url('inventory_controller/edit/' . $i['id']); ?>">Edit</a>
                                        |
                                        <a href="<?php echo site_url('inventory_controller/remove/' . $i['id']); ?>">Delete</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->