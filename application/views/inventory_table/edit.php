<link rel="stylesheet" type="text/css"
      href="<?php echo base_url() ?>plugins/datetimepicker/jquery.datetimepicker.min.css"/>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/select2/select2.min.css">

<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ADD PRODUCT
            <small>add a new product</small>
        </h1>
        <?php $this->load->view('/flashMessage'); ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="register-box">
            <div class="register-box-body">


                <?php echo form_open('inventory_controller/edit/' . $inventory_table['id'], ['id' => 'add']); ?>

                <div>
                    Product Name :
                    <select name="product_id" class="form-control" required>
                        <option value="">select product_table</option>
                        <?php
                        foreach ($all_product_table as $product_table) {
                            $selected = ($product_table['product_id'] == $inventory_table['product_id']) ? ' selected="selected"' : "";

                            echo '<option value="' . $product_table['product_id'] . '" ' . $selected . '>' . $product_table['product_name'] . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div>Product Purchase Rate <sub>per kg</sub> <input type="text" class="form-control"
                                                                    id="product_purchase_price"
                                                                    name="product_purchase_price"
                                                                    value="<?php echo($this->input->post('product_purchase_price') ? $this->input->post('product_purchase_price') : $inventory_table['purchase_rate']); ?>"
                                                                    required/>
                </div>
                <div>Product Quantity <sub> kg</sub> <input type="text" class="form-control" id="product_quantity"
                                                            name="product_quantity"
                                                            value="<?php echo($this->input->post('product_quantity') ? $this->input->post('product_quantity') : $inventory_table['product_quantity']); ?>"
                                                            required/>
                </div>
                <div>Purchase Date : <input type="text" class="form-control" id="purchaseDate" name="purchase_date"
                                            value="<?php echo($this->input->post('purchase_date') ? $this->input->post('purchase_date') : $inventory_table['purchase_date']); ?>"
                                            required/>
                </div>
                <div>Purchase Total Amount : <input type="text" class="form-control" id="totalAmount"
                                                    name="purchase_total_amount"
                                                    value="<?php echo($this->input->post('purchase_total_amount') ? $this->input->post('purchase_total_amount') : $inventory_table['purchase_total_amount']); ?>"
                                                    required/>
                </div>

                <br>
                <div class="row">
                    <div class="col-xs-8">
                        <span class="error"><?php echo validation_errors(); ?></span>
                    </div>
                    <div class="col-xs-4">

                        <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
            <!-- /.form-box -->
        </div>

    </section>
    <!-- /.content -->
</div>

<script>
    $('#purchaseDate').datetimepicker({
        format: 'Y-m-d',
        step: 30
    });

</script>
<script type="text/javascript">
    $("#product_quantity").keyup(function () {
        var quantity = $('#product_quantity').val();
        var price = $('#product_purchase_price').val();
        var total = (quantity * price);


        $('#totalAmount').val(total);
    });

    $("#add").validate({});
</script>


