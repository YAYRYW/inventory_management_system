<?php


class Inventory_table_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get inventory_table by id
     */
    function get_inventory_table($id)
    {
        return $this->db->get_where('inventory_table', array('id' => $id))->row_array();
    }

    /*
     * Get all inventory_table
     */
    function get_all_inventory_table()
    {
        return $this->db->get('inventory_table')->result_array();
    }

    /*
     * function to add new inventory_table
     */
    function add_inventory_table($params)
    {
        $this->db->insert('inventory_table', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update inventory_table
     */
    function update_inventory_table($id, $params)
    {
        $this->db->where('id', $id);
        $response = $this->db->update('inventory_table', $params);
        if ($response) {
            return "inventory_table updated successfully";
        } else {
            return "Error occuring while updating inventory_table";
        }
    }

    /*
     * function to delete inventory_table
     */
    function delete_inventory_table($id)
    {
        $response = $this->db->delete('inventory_table', array('id' => $id));
        if ($response) {
            return "inventory_table deleted successfully";
        } else {
            return "Error occuring while deleting inventory_table";
        }
    }

    function get_all_inventory_table_data()
    {
        $query = $this->db->query("
                                 SELECT 
                                 SUM(product_quantity),
                                 `product_id`,
                                 SUM(`purchase_total_amount`) 
                                 FROM `inventory_table` 
                                 Group by product_id 
                                 ");


        return $query->result_array();
    }

    function get_five_inventory_table_data()
    {
        $query = $this->db->query("
                                 SELECT 
                                 SUM(product_quantity),
                                 `product_id`,
                                 SUM(`purchase_total_amount`) 
                                 FROM `inventory_table` 
                                 Group by product_id 
                                 limit 5 
                                 ");


        return $query->result_array();
    }

    function purchase_amount()
    {
        $query = $this->db->query("
                                 SELECT 
                                 SUM(`purchase_total_amount`) as total 
                                 FROM `inventory_table` 
                                 ");


        return @$query->result_array()[0];
    }

    function indevidual_product_qty($id)
    {
        $query = $this->db->query("
                                 SELECT
                                 SUM(product_quantity) AS qty, 
                                 SUM(`purchase_total_amount`) AS ta  
                                 FROM `inventory_table`
                                 WHERE product_id=$id
                                 ");

//print_r($query);
        return $query->result_array()[0];
    }


}
