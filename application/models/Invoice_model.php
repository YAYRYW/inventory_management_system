<?php

/*
 * Generated by CRUDigniter v3.0 Beta 
 * www.crudigniter.com
 */

class Invoice_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get invoice by id
     */
    function get_invoice($id)
    {
        return $this->db->get_where('invoice', array('invoice_no' => $id))->row_array();
    }

    /*
     * Get all invoice
     */
    function get_all_invoice()
    {
        return $this->db->get('invoice')->result_array();
    }

    /*
     * function to add new invoice
     */
    function add_invoice($params)
    {
        $this->db->insert('invoice', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update invoice
     */
    function update_invoice($id, $params)
    {
        $this->db->where('invoice_no', $id);
        $response = $this->db->update('invoice', $params);
        if ($response) {
            return "invoice updated successfully";
        } else {
            return "Error occuring while updating invoice";
        }
    }

    /*
     * function to delete invoice
     */
    function delete_invoice($id)
    {
        $response = $this->db->delete('invoice', array('id' => $id));
        if ($response) {
            return "invoice deleted successfully";
        } else {
            return "Error occuring while deleting invoice";
        }
    }

    function get_invoice_data($invoiceNo)
    {
        $query = $this->db->query("
                                  SELECT * 
                                  FROM invoice
                                  WHERE  invoice_no = $invoiceNo ");

        return $query->result_array()[0];
    }

    function lastInsertId()
    {
        $query = $this->db->query("
                                  SELECT id
                                  FROM invoice ORDER BY id DESC LIMIT 1
                                  ");

        return @$query->result_array()[0];
    }

    function get_all_invoice_data()
    {
        $query = $this->db->query("
                                  SELECT * 
                                  FROM invoice
                                  ORDER BY id DESC 
                                  limit 7   ");

        return $query->result_array();
    }

    function GetIncome()
    {
        $query = $this->db->query("
                                  SELECT 
                                  SUM(`subTotal`) AS grossAmount                                
                                  FROM `invoice` 
                                  ");


        return @$query->result_array()[0];
//        return $this->db->last_query();
    }
}