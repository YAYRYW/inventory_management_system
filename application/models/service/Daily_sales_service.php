<?php

class Daily_sales_service extends CI_Model
{
    function __construct()
    {
        parent::__construct();

    }


    function get_product_list_by_product_type($product_name)
    {
        $query = $this->db->query("
                                    SELECT i.*, p.* 
                                    FROM `inventory_table` i
                                    JOIN product_table p 
                                    ON i.product_id=p.product_id
                                     WHERE p.product_name 
                                     Like '$product_name%' GROUP BY p.product_id");


        return $query->result_array();
//        return $this->db->last_query();
    }

    function get_shop_list_by_shop_name($shop_name)
    {
        $query = $this->db->query("SELECT * 
                                    FROM shop_table  
                                    WHERE 
                                     shop_name Like '$shop_name%'");


        return $query->result_array();
//        return $this->db->last_query();
    }

    function get_delivery_man_list($name)
    {
        $query = $this->db->query("SELECT * 
                                    FROM delivery_man_table  
                                    WHERE 
                                     full_name Like '$name%'");


        return $query->result_array();
//        return $this->db->last_query();
    }

    function GetReportData($fromDate, $toDate)
    {
        $query = $this->db->query("
                                  SELECT 
                                  SUM(`subTotal`) AS grossAmount,
                                  SUM(`paidAmount`) AS saleAmount,
                                  SUM(`due`) AS DueAmount
                                  FROM `invoice` 
                                  WHERE `invoice_date` 
                                  BETWEEN '$fromDate' AND '$toDate' ");


        return @$query->result_array()[0];
//        return $this->db->last_query();
    }

    function GetPurchasePrice($fromDate, $toDate)
    {
        $query = $this->db->query("
                                  SELECT 
                                  SUM(`purchase_total_amount`) AS purchaseAmount
            
                                  FROM `inventory_table` 
                                  WHERE `purchase_date` 
                                  BETWEEN '$fromDate' AND '$toDate' ");


        return @$query->result_array()[0];
//        return $this->db->last_query();
    }

    function GetExpenseData($fromDate, $toDate)
    {
        $query = $this->db->query("
                                  SELECT SUM(`expense_amount`) AS expense_amount
                                  FROM `daily_expense_table`
                                  WHERE `expense_date` 
                                  BETWEEN '$fromDate' AND '$toDate' ");

        if (!$query) {
            echo "No Expense";
        } else {
            return @$query->result_array()[0];
//        return $this->db->last_query();
        }
    }

}