<?php

class Product_service extends CI_Model
{
    function __construct()
    {
        parent::__construct();

    }


    function getTotalProductQuantityByProductId($product_id)
    {
        $query = $this->db->query("
                                    SELECT SUM(product_quantity) AS totalProduct 
                                    FROM `inventory_table` 
                                    WHERE product_id= $product_id ");


        return @$query->result_array()[0];

    }

    function getSoldReturnedDamageProductAmount($product_id)
    {
        $query = $this->db->query("
                                  SELECT SUM(invoice_product_qty) AS TotalSold
                                        
                                         FROM `order_table` 
                                         WHERE product_id= $product_id ");


        return @$query->result_array()[0];

    }

    function Getpercentage($x, $y)
    {
        $percent = $x / $y;
        $percent_friendly = number_format($percent * 100, 2) . '%'; // change 2 to # of decimals
        return $percent_friendly;
    }


}