<?php

/*
 * Generated by CRUDigniter v3.0 Beta 
 * www.crudigniter.com
 */

class Product_table_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get product_table by product_id
     */
    function get_product_table($product_id)
    {
        return $this->db->get_where('product_table', array('product_id' => $product_id))->row_array();
    }

    /*
     * Get all product_table
     */
    function get_all_product_table()
    {
        return $this->db->get('product_table')->result_array();
    }

    /*
     * function to add new product_table
     */
    function add_product_table($params)
    {
        $this->db->insert('product_table', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update product_table
     */
    function update_product_table($product_id, $params)
    {
        $this->db->where('product_id', $product_id);
        $response = $this->db->update('product_table', $params);
        if ($response) {
            return "product_table updated successfully";
        } else {
            return "Error occuring while updating product_table";
        }
    }

    /*
     * function to delete product_table
     */
    function delete_product_table($product_id)
    {
        $response = $this->db->delete('product_table', array('product_id' => $product_id));
        if ($response) {
            return "product_table deleted successfully";
        } else {
            return "Error occuring while deleting product_table";
        }
    }

    function get_product_price($product_id)
    {
        $query = $this->db->query("
                                  SELECT product_purchase_price 
                                  FROM product_table
                                  WHERE  product_id = $product_id ");

        return @$query->result_array()[0];
    }

    function get_product_list()
    {
        $query = $this->db->query("
                                  SELECT p.product_name, p.product_id FROM product_table p JOIN inventory_table i ON p.product_id = i.`product_id`  GROUP BY  i.`product_id`
                                   ");

        return $query->result_array();
    }
}
