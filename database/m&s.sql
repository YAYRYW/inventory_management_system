-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2017 at 02:28 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `m&s`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_table`
--

CREATE TABLE IF NOT EXISTS `customer_table` (
`id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_number` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_table`
--

INSERT INTO `customer_table` (`id`, `company_name`, `full_name`, `address`, `contact_number`) VALUES
(1, 'Invi IT', 'Shah Faisal', 'North Badda, Dhaka', '01673601675'),
(2, 'Jihan Emon', 'Spark Tech', 'Banasree, Dhaka', '01256666666'),
(3, 'Samsung R&D Institute Bangladesh', 'Khairul Hasan', 'Kawran Bazar, Dhaka', '01673601675');

-- --------------------------------------------------------

--
-- Table structure for table `daily_expense_table`
--

CREATE TABLE IF NOT EXISTS `daily_expense_table` (
`expense_id` int(11) NOT NULL,
  `expense_details` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `expense_amount` int(11) NOT NULL,
  `expense_date` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `expenses_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logged_in_user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `daily_expense_table`
--

INSERT INTO `daily_expense_table` (`expense_id`, `expense_details`, `expense_amount`, `expense_date`, `expenses_by`, `logged_in_user_id`) VALUES
(1, 'Tea', 20, '21-03-2017 ', '1', 1),
(2, 'Lunch', 200, '23-03-2017', '1', 1),
(3, 'Tea', 50, '11-05-2017', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_table`
--

CREATE TABLE IF NOT EXISTS `inventory_table` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_quantity` varchar(100) NOT NULL,
  `purchase_date` varchar(100) NOT NULL,
  `purchase_rate` double(10,2) NOT NULL,
  `purchase_total_amount` double(10,2) NOT NULL,
  `login_user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_table`
--

INSERT INTO `inventory_table` (`id`, `product_id`, `product_quantity`, `purchase_date`, `purchase_rate`, `purchase_total_amount`, `login_user_id`) VALUES
(1, 1, '500', '16-3-2017', 0.00, 22500.00, 1),
(2, 2, '1800', '16-3-2017', 0.00, 158400.00, 1),
(3, 3, '1000', '16-3-2017', 0.00, 13500.00, 1),
(4, 4, '50', '16-3-2017', 0.00, 9900.00, 1),
(5, 5, '10000', '16-3-2017', 0.00, 24600.00, 1),
(6, 9, '1000', '16-3-2017', 0.00, 17000.00, 1),
(7, 11, '10000', '16-3-2017', 0.00, 2500.00, 1),
(8, 12, '1000', '20-03-2017', 0.00, 50000.00, 1),
(9, 4, '5000', '20-03-2017', 0.00, 990000.00, 1),
(10, 3, '2', '24-03-2017', 0.00, 270.00, 1),
(11, 9, '5000', '21-03-2017', 0.00, 85000.00, 1),
(12, 17, '2500', '21-03-2017', 0.00, 47500.00, 1),
(13, 13, '568', '21-03-2017', 0.00, 28400.00, 1),
(14, 21, '3589', '21-03-2017', 0.00, 179450.00, 1),
(15, 13, '5000', '21-03-2017', 0.00, 250000.00, 1),
(16, 23, '5000', '23-03-2017', 0.00, 286100.00, 1),
(17, 24, '10', '11-05-2017', 50.00, 600.00, 1),
(18, 24, '200', '11-05-2017', 0.00, 10000.00, 1),
(19, 1, '600', '11-05-2017', 0.00, 35400.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
`id` int(11) NOT NULL,
  `invoice_no` varchar(50) NOT NULL,
  `invoice_date` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `subTotal` double(10,2) NOT NULL,
  `paidAmount` double NOT NULL,
  `due` double NOT NULL,
  `discount` double NOT NULL,
  `remarks` text NOT NULL,
  `status` int(11) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `invoice_no`, `invoice_date`, `customer_id`, `subTotal`, `paidAmount`, `due`, `discount`, `remarks`, `status`, `user`) VALUES
(9, '1705111', '11-05-2017 2:27 PM', 1, 8338.00, 8338, 0, 0, 'paid', 1, 1),
(10, '17051110', '11-05-2017 2:29 PM', 2, 1484.00, 1484, 0, 0, 'ghf', 1, 1),
(11, '17051111', '11-05-2017 3:30 PM', 2, 147.00, 147, 0, 0, 'v', 1, 1),
(12, '17051112', '11-05-2017 4:42 PM', 2, 176200.00, 176200, 0, 0, '11-05-2017 5:54 PM', 1, 1),
(13, '17051113', '11-05-2017 4:46 PM', 1, 9900.00, 9900, 0, 0, 'zXCz', 1, 1),
(14, '17051114', '11-05-2017 5:56 PM', 3, 500.00, 500, 0, 0, 'paid', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_table`
--

CREATE TABLE IF NOT EXISTS `order_table` (
`id` int(11) NOT NULL,
  `invoice_no` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `invoice_product_qty` int(11) NOT NULL,
  `invoice_product_price` float NOT NULL,
  `invoice_product_discount` float NOT NULL,
  `invoice_product_sub` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_table`
--

INSERT INTO `order_table` (`id`, `invoice_no`, `customer_id`, `product_id`, `invoice_product_qty`, `invoice_product_price`, `invoice_product_discount`, `invoice_product_sub`) VALUES
(18, '17051111', 2, 3, 13, 6, 0, 78),
(19, '17051111', 2, 11, 13, 3, 0, 39),
(20, '17051111', 2, 19, 10, 3, 0, 30),
(21, '17051112', 2, 24, 20, 60, 0, 1200),
(22, '17051112', 2, 1, 250, 700, 0, 175000),
(23, '17051113', 1, 1, 300, 33, 0, 9900),
(24, '17051114', 3, 12, 10, 50, 0, 500);

-- --------------------------------------------------------

--
-- Table structure for table `payment_table`
--

CREATE TABLE IF NOT EXISTS `payment_table` (
`id` int(11) NOT NULL,
  `invoice_no` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `invoice_date` varchar(100) NOT NULL,
  `total_amount` float NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_table`
--

INSERT INTO `payment_table` (`id`, `invoice_no`, `customer_id`, `invoice_date`, `total_amount`, `user`) VALUES
(12, '1705111', 1, '11-05-2017 2:27 PM', 8338, 1),
(13, '17051110', 2, '11-05-2017 2:29 PM', 1484, 1),
(14, '17051111', 2, '11-05-2017 3:30 PM', 147, 1),
(15, '17051112', 2, '11-05-2017 4:42 PM', 100000, 1),
(16, '17051113', 1, '11-05-2017 4:46 PM', 9900, 1),
(17, '17051112', 0, '11-05-2017 5:54 PM', 76200, 1),
(18, '17051114', 3, '11-05-2017 5:56 PM', 500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_table`
--

CREATE TABLE IF NOT EXISTS `product_table` (
`product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_table`
--

INSERT INTO `product_table` (`product_id`, `product_name`) VALUES
(1, ' Aerosol'),
(2, 'ACI Aerosol'),
(3, 'ACI Aerosol'),
(4, 'ACI Aerosol'),
(5, 'ACI Aerosol'),
(6, 'ACI Aerosol'),
(7, 'ACI Cockroach Spray '),
(8, 'ACI Cockroach Spray '),
(9, 'ACI high booster coil'),
(10, 'ACI black fighter coil'),
(11, 'ACI Tsunami coil'),
(12, 'Sports soap'),
(13, 'Men soap'),
(14, 'Vanish toilet cleaner'),
(15, 'Vanish Advanced Gel'),
(16, 'Vanish toilet cleaner'),
(17, 'Vanish powder'),
(18, 'vanish powder'),
(19, 'Smart washing powder'),
(20, 'Smart washing powder'),
(21, 'Smart washing powder'),
(22, 'Colgate dental cream'),
(23, 'Colgate dental cream'),
(24, 'Burger');

-- --------------------------------------------------------

--
-- Table structure for table `user_table`
--

CREATE TABLE IF NOT EXISTS `user_table` (
`user_id` int(11) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `mobile_no` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_type_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_table`
--

INSERT INTO `user_table` (`user_id`, `full_name`, `mobile_no`, `user_name`, `address`, `user_password`, `user_type_id`) VALUES
(1, 'admin', '01673601675', 'admin', 'Khilgaon,Dhaka', '123123', 1),
(2, 'Jihan', '012555666663', 'jihan.inviit', 'Dhaka', '000000', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_type_table`
--

CREATE TABLE IF NOT EXISTS `user_type_table` (
`id` int(11) NOT NULL,
  `user_type` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type_table`
--

INSERT INTO `user_type_table` (`id`, `user_type`) VALUES
(1, 'Admin'),
(2, 'Manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_table`
--
ALTER TABLE `customer_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_expense_table`
--
ALTER TABLE `daily_expense_table`
 ADD PRIMARY KEY (`expense_id`);

--
-- Indexes for table `inventory_table`
--
ALTER TABLE `inventory_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_table`
--
ALTER TABLE `order_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_table`
--
ALTER TABLE `payment_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_table`
--
ALTER TABLE `product_table`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `user_table`
--
ALTER TABLE `user_table`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_type_table`
--
ALTER TABLE `user_type_table`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_table`
--
ALTER TABLE `customer_table`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `daily_expense_table`
--
ALTER TABLE `daily_expense_table`
MODIFY `expense_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inventory_table`
--
ALTER TABLE `inventory_table`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `order_table`
--
ALTER TABLE `order_table`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `payment_table`
--
ALTER TABLE `payment_table`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `product_table`
--
ALTER TABLE `product_table`
MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `user_table`
--
ALTER TABLE `user_table`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_type_table`
--
ALTER TABLE `user_type_table`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
